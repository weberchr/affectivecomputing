# Real Talk - An app that knows your true motivations

This Android chat app was developed in the Affective Computing Master's practical course at the LMU in cooperation with HYVE that monitors and visually displays the emotions of users during chatting.

- The recognition of emotions was realised with a neural network in Python based on TensorFlow.
- While the user is typing, the face is analysed 25 frames/second via the front camera.
- When the text message is sent, a sophisticated average of all recognised emotions is determined, adapted to the user.
- The text message is coloured according to the recognised emotion.
- The user can manually change the colour if the recognition is incorrect.

![presentation](/.img/RealTalk.jpg)


## Team Members
- Adina Klink
- Christoph Weber
- Viktoria Pezzei
