package com.affectivecomputing.emotionmonitoringapp.ui.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.affectivecomputing.emotionmonitoringapp.R;
import com.affectivecomputing.emotionmonitoringapp.emotionClassifier.MainCameraFragment;
import com.affectivecomputing.emotionmonitoringapp.models.Chat;
import com.affectivecomputing.emotionmonitoringapp.ui.fragments.ChatFragment;
import com.affectivecomputing.emotionmonitoringapp.utils.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;


public class ChatRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_ME = 1;
    private static final int VIEW_TYPE_OTHER = 2;

    private static List<Chat> mChats;

    public ChatRecyclerAdapter(List<Chat> chats) {
        mChats = chats;
    }

    public void add(Chat chat) {
        mChats.add(chat);
        notifyItemInserted(mChats.size() - 1);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case VIEW_TYPE_ME:
                View viewChatMine = layoutInflater.inflate(R.layout.item_chat_mine, parent, false);
                viewHolder = new MyChatViewHolder(viewChatMine);
                break;
            case VIEW_TYPE_OTHER:
                View viewChatOther = layoutInflater.inflate(R.layout.item_chat_other, parent, false);
                viewHolder = new OtherChatViewHolder(viewChatOther);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (TextUtils.equals(mChats.get(position).senderUid,
                FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            configureMyChatViewHolder((MyChatViewHolder) holder, position);

        } else {
            configureOtherChatViewHolder((OtherChatViewHolder) holder, position);
        }
    }

    // sender's messages
    private void configureMyChatViewHolder(MyChatViewHolder myChatViewHolder, int position) {
        Chat chat = mChats.get(position);
        System.out.println("OLD: " + chat);

        String alphabet = chat.sender.substring(0, 1);

        myChatViewHolder.txtChatMessage.setText(chat.message);
        myChatViewHolder.txtUserAlphabet.setText(alphabet);

        // change color of chat bubble based on current emotion
        myChatViewHolder.txtChatMessage.getBackground().setColorFilter(Color.parseColor(chat.color), PorterDuff.Mode.SRC_ATOP);

        // change color on long click
        myChatViewHolder.txtChatMessage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final CharSequence[] items = {"Happy", "Sad", "Angry", "Neutral"};

                AlertDialog.Builder builder = new AlertDialog.Builder(myChatViewHolder.txtChatMessage.getContext());

                builder.setTitle("Change your emotion");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            chat.color = "#FBE97A";
                        } else if (item == 1) {
                            chat.color = "#89A8FB";
                        } else if (item == 2) {
                            chat.color = "#FB9292";
                        } else if (item == 3) {
                            chat.color = "#D5D4D4";
                        }

                        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                        final String room_type_1 = chat.senderUid + "_" + chat.receiverUid;
                        final String room_type_2 = chat.receiverUid + "_" + chat.senderUid;

                        databaseReference.child(Constants.ARG_CHAT_ROOMS).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.hasChild(room_type_1)) {

                                    final DatabaseReference colorData = databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_1).child(String.valueOf(chat.timestamp)).child("color");
                                    colorData.setValue(chat.color);

                                    myChatViewHolder.txtChatMessage.getBackground().setColorFilter(Color.parseColor(chat.color), PorterDuff.Mode.SRC_ATOP);

                                } else if (dataSnapshot.hasChild(room_type_2)) {

                                    final DatabaseReference colorData = databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_2).child(String.valueOf(chat.timestamp)).child("color");
                                    colorData.setValue(chat.color);

                                    myChatViewHolder.txtChatMessage.getBackground().setColorFilter(Color.parseColor(chat.color), PorterDuff.Mode.SRC_ATOP);

                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }

                        });
                    }
                });
                builder.show();
                return true;
            }
        });
    }

    // receiving messages
    private void configureOtherChatViewHolder(OtherChatViewHolder otherChatViewHolder, int position) {
        Chat chat = mChats.get(position);

        String alphabet = chat.sender.substring(0, 1);

        otherChatViewHolder.txtChatMessage.setText(chat.message);
        otherChatViewHolder.txtUserAlphabet.setText(alphabet);

        // set color based on emotion
        otherChatViewHolder.txtChatMessage.getBackground().setColorFilter(Color.parseColor(chat.color), PorterDuff.Mode.SRC_ATOP);
    }


    public static String changeColor() {
        // change color dynamically
        if (MainCameraFragment.getOverallEmotion().equals("sad")) {
            return "#89A8FB";
        } else if (MainCameraFragment.getOverallEmotion().equals("happy")) {
            return "#FBE97A";
        } else if (MainCameraFragment.getOverallEmotion().equals("angry")) {
            return "#FB9292";
        } else {
            return "#D5D4D4";
        }
    }

    @Override
    public int getItemCount() {
        if (mChats != null) {
            return mChats.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (TextUtils.equals(mChats.get(position).senderUid,
                FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            return VIEW_TYPE_ME;
        } else {
            return VIEW_TYPE_OTHER;
        }
    }

    public static class MyChatViewHolder extends RecyclerView.ViewHolder {
        public static TextView txtChatMessage;
        private TextView txtUserAlphabet;


        private MyChatViewHolder(View itemView) {
            super(itemView);
            txtChatMessage = (TextView) itemView.findViewById(R.id.text_view_chat_message);
            txtUserAlphabet = (TextView) itemView.findViewById(R.id.text_view_user_alphabet);
        }
    }

    public static class OtherChatViewHolder extends RecyclerView.ViewHolder {
        public static TextView txtChatMessage;
        private TextView txtUserAlphabet;

        public OtherChatViewHolder(View itemView) {
            super(itemView);
            txtChatMessage = (TextView) itemView.findViewById(R.id.text_view_chat_message);
            txtUserAlphabet = (TextView) itemView.findViewById(R.id.text_view_user_alphabet);
        }
    }
}
