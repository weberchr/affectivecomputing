import os
import librosa
import speechpy
import numpy as np
from shutil import copyfile
import tensorflow as tf
from tensorflow.python.keras.layers import LSTM, Dropout, Dense
# from keras.layers import CuDNNLSTM
from sklearn.model_selection import train_test_split

np.random.seed(42)
tf.set_random_seed(42)


def preprocess_audio_file(filepath,
                          n_mfcc=39,
                          raw_audio=False,
                          spectogram=False):
    """
    Preprocess audio file

    :param filepath: path to .wav-file
    :param n_mfcc: Number of mfcc features to take for each frame (e.g., for RNN)
    :param raw_audio: Whether to return raw audio signal (e.g., for Conv1D neural network)
    :param spectogram: Whether to return a melspectogram (e.g., for Conv2D neural network)

    :return feature vector from mfcc
    """
    # step 2
    signal, sr = librosa.load(path=filepath, sr=16000)
    s_len = len(signal)

    # empirically calculated mean length for the given data set (2 seconds)
    mean_signal_length = 32000  # 16kHz * 2 seconds = 32kHz

    # pad/slice the signals to have same size if lesser than required
    if s_len < mean_signal_length:
        pad_len = mean_signal_length - s_len
        pad_rem = pad_len % 2
        pad_len //= 2
        signal = np.pad(signal, (pad_len, pad_len + pad_rem),
                        'constant', constant_values=0)
    else:
        pad_len = s_len - mean_signal_length
        pad_len //= 2
        signal = signal[pad_len:pad_len + mean_signal_length]

    # return raw audio signal
    if raw_audio:
        return signal

    # parameters for STFT
    n_fft = 512
    hop_length = 256
    frame_length = n_fft / sr
    frame_stride = hop_length / sr

    # amount mel bands
    n_mels = 40

    # compute mel scale spectogram
    if spectogram:
        S = librosa.feature.melspectrogram(y=signal,
                                           sr=sr,
                                           n_fft=n_fft,
                                           hop_length=hop_length,
                                           n_mels=n_mels)
        S = librosa.power_to_db(S)
        return S

    # step 3
    # compute MFCCs
    mfccs = speechpy.feature.mfcc(signal=signal,
                                  sampling_frequency=sr,
                                  frame_length=frame_length,
                                  frame_stride=frame_stride,
                                  fft_length=n_fft,
                                  num_filters=n_mels,
                                  num_cepstral=n_mfcc)
    return mfccs


def get_train_test_dataset(path,
                           class_labels,
                           n_mfcc=39,
                           raw_audio=False,
                           spectogram=False,
                           test_size=0.2):
    """
    Extract data for training and testing.

    :param path: directory to files
    :param class_labels: list of labels
    :param n_mfcc: Number of mfcc features to take for each frame
    :param raw_audio: Whether to use raw audio signal
    :param spectogram: Whether to use melspectograms
    :param test_size: Fraction of data to use for testing

    :return X_train, y_train, X_test, y_test (feature vectors with labels)
    """
    data = []  # X
    labels = []  # y

    # step 1
    # iterate over audio files and get mfcc feature vector
    for i, label in enumerate(class_labels):
        directory = os.path.join(path, label)
        for filename in os.listdir(directory):
            filepath = os.path.join(directory, filename)
            # step 2 + 3
            # shape: (n_windows, n_mfccs)
            feature_vector = preprocess_audio_file(filepath=filepath,
                                                   raw_audio=raw_audio,
                                                   spectogram=spectogram,
                                                   n_mfcc=n_mfcc)
            # step 4
            data.append(feature_vector)
            labels.append(i)
    # step 5
    # split into train/test
    data = np.array(data)
    labels = np.array(labels)
    # one-hot encoding => e.g., label=5 => [0,0,0,0,0,1,0] (labels from 0-6)
    labels = tf.keras.utils.to_categorical(y=labels, num_classes=len(class_labels))  # dtype=int

    # X[0]: [[1, 2, 9, 20, ... , 2], [1, 2, 9, 20, ... , 2], [...]]
    # y[0]: [1,0,0,0,0,0,0]
    X_train, X_test, y_train, y_test = train_test_split(
        data,  # X (10% angry, 5% sad, ...) -> train (10% angry), test (10% angry)
        labels,  # y
        stratify=labels,
        test_size=test_size,
        shuffle=True,
        random_state=42)

    return X_train, X_test, y_train, y_test


def build_rnn_model(input_shape, num_classes):
    # keras linear stack of layers
    model = tf.keras.Sequential([
        LSTM(128, input_shape=input_shape),
        # CuDNNGRU(128, input_shape=input_shape), # high performant alternative to LSTMs
        Dropout(rate=0.5),
        Dense(32, activation='relu'),
        Dense(16, activation='relu'),
        Dense(num_classes, activation='softmax')
    ])

    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    print(model.summary())
    return model


def main():
    # `.wav` files are named as:
    # Positions 1-2: number of speaker
    # Positions 3-5: code for text
    # Position 6: emotion (sorry, letter stands for german emotion word)
    # Position 7: if there are more than two versions these are numbered a, b, c

    emotions = {
        'W': 'anger',
        'L': 'boredom',
        'E': 'disgust',
        'A': 'fear',
        'F': 'happiness',
        'T': 'sadness',
        'N': 'neutral'
    }

    # create dirs for emotions
    audiopath = "../../data/audio"

    base_path = 'emodb/data'
    base_path = os.path.join(audiopath, base_path)
    for e in emotions.values():
        directory = os.path.join(base_path, e)
        if not os.path.exists(directory):
            os.makedirs(directory)

    # put .wav files into respective directories
    wav_path = 'emodb/wav'
    wav_path = os.path.join(audiopath, wav_path)

    for file in os.listdir(path=wav_path):
        dst = os.path.join(base_path, emotions[file[-6]], file)
        copyfile(os.path.join(wav_path, file), dst)
    X_train, X_test, y_train, y_test = get_train_test_dataset(base_path,
                                                              emotions.values())

    print('Shape of X_train: {} -> (num_samples, num_timesteps, num_features)'.format(X_train.shape))
    print('Shape of y_train: {} -> (num_samples, num_classes)'.format(y_train.shape))

    model = build_rnn_model((X_train.shape[1], X_train.shape[2]), y_train.shape[1])
    model.fit(x=X_train,
              y=y_train,
              shuffle=True,
              batch_size=32,
              epochs=20,
              validation_data=(X_test, y_test))

    score, acc = model.evaluate(X_test, y_test)

    print(score, acc)

    model_path = '../../data/models/audio_model.h5'
    model.save(model_path)


if __name__ == '__main__':
    main()
