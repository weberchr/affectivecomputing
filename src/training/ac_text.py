import pickle
import numpy as np
from collections import Counter
import tensorflow as tf
tf_session = tf.Session(config=tf.ConfigProto(inter_op_parallelism_threads=1))
from keras.models import Sequential
from keras.layers import Embedding, GlobalMaxPooling1D, Dense, Conv1D, Dropout, Flatten, Bidirectional, LSTM
from keras import backend as K
K.set_session(tf_session)
from sklearn.preprocessing import LabelEncoder
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split


#benötigte Pfade zu
data_path = "../../data/text"


def build_model(tsv_file):
    """ Builds, trains and evaluates a keras model. """

    train_tokens, train_labels = tokens_and_labels(tsv_file)
    train_tokens, dev_tokens, train_labels, dev_labels = train_test_split(train_tokens, train_labels, test_size=0.3)

    # Convert text and labels to matrix format.
    VOCAB_SIZE = 10000
    MAX_LEN = 100
    BATCH_SIZE = 32
    EPOCHS = 10
    num_classes = 10

    word_to_id = get_word_to_id_map(train_tokens, VOCAB_SIZE)
    le = LabelEncoder()
    le.fit(train_labels)

    train_matrix = get_text_matrix(train_tokens, word_to_id, MAX_LEN)
    train_labels = to_categorical(le.transform(train_labels), num_classes)
    dev_matrix = get_text_matrix(dev_tokens, word_to_id, MAX_LEN)
    dev_labels = to_categorical(le.transform(dev_labels), num_classes)

    #Hinzufügen von Layern
    model = Sequential()
    # model.add(Bidirectional(LSTM(units=25)))
    model.add(Embedding(input_dim=VOCAB_SIZE, output_dim=100))
    model.add(Dropout(0.25))
    model.add(Conv1D(activation='relu', filters=25, kernel_size=3))
    model.add(GlobalMaxPooling1D())
    model.add(Dense(num_classes, activation='softmax'))

    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    #model.fit(train_matrix, train_labels, validation_data=(dev_matrix, dev_labels), epochs=EPOCHS, batch_size=BATCH_SIZE)
    model.fit(train_matrix, train_labels, validation_data=(dev_matrix, dev_labels), epochs=EPOCHS)

    # y_predicted_dev = model.predict_classes(dev_matrix)
    # predicted_relation_names = le.inverse_transform(y_predicted_dev)

    score, acc = model.evaluate(dev_matrix, dev_labels)

    return model, score, acc


def tokens_and_labels(filename):
    """ This reads a file with relation/sentence instances (in tab-sepated format) and returns two lists:
    textlist: list of strings; The white-space seprated tokens.
    labellist: list of strings; The relation names for all instances.
    """
    textlist = []
    labellist = []
    with open(filename, encoding='utf-8') as f:
        for line in f:
            id, category, post = line.strip().split('\t')
            textlist.append(post)
            labellist.append(category)
    return textlist, labellist

def get_word_to_id_map(textlist, vocab_size):
    """ Creates mapping word -> id for the most frequent words in the vocabulary. Ids 0 and 1 are reserved for the
    padding symbol <PAD> and the unknown token <UNK>. vocab_size determines the overall vocabulary size (including <UNK>
    and <PAD>)"""
    assert(vocab_size >= 2)
    c = Counter(tok for text in textlist for tok in text.split(" "))
    try:
        # Use fixed word frequencies
        with open(data_path + "/ids_words" + str(vocab_size) + ".p", "rb") as f:
            ids_words = pickle.load(f)
    except FileNotFoundError:
        ids_words = enumerate(['<PAD>', '<UNK>'] + sorted([word for word, count in c.most_common(vocab_size - 2)]))
        # Write file instead of extensive deterministic function
        with open(data_path + "/ids_words" + str(vocab_size) + ".p", "wb") as f:
            pickle.dump(ids_words, f)
    return {w: idx for idx, w in ids_words}

def get_text_matrix(textlist, word_to_id, maxlen):
    """ This takes textlist (list with white-space separated tokens) and returns a numpy matrix of size
    len(textlist) x maxlen.
    Each row in the matrix contains for each text the sequence of word ids (i.e. the columns correspond to the positions
    in the sentence).
    If a sentence is longer than maxlen, it is truncated after maxlen tokens.
    If a sentence is shorter than maxlen, it is filled with 0 (= the word id of the <PAD> token).
    """
    m = np.zeros((len(textlist), maxlen), dtype=int)
    row_nr, col_nr = 0, 0
    for text in textlist:
        col_nr = 0
        for word in text.split(" "):
            if col_nr == maxlen:
                break
            m[row_nr, col_nr] = word_to_id.get(word, 1)  # id for <UNK>
            col_nr += 1
        row_nr += 1
    return m


# Ausführen des Trainingsschritts
def main(tsv_file):
    model, score, acc = build_model(tsv_file)
    print('Accuracy of model:', acc)
    #model.save('../../data/models/text_model.h5')
    print('done')

if __name__ == '__main__':
    tsv_file = '../../data/text/mpc.tsv'
    main(tsv_file)
