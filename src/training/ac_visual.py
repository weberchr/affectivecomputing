import keras
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, AveragePooling2D, ZeroPadding2D, Convolution2D, Input
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator
from keras.layers.normalization import BatchNormalization
import numpy as np
from keras.utils.np_utils import to_categorical
from keras.models import Model
from keras.models import load_model
import pandas as pd
from keras.applications.vgg16 import VGG16


batch_size = 64
img_width,img_height = 48, 48

# 0=Angry, 1=Disgust, 2=Fear, 3=Happy, 4=Sad, 5=Surprise, 6=Neutral
num_classes = 7

def _load_fer(CSV_FILE):
    # Load training and eval data
    df = pd.read_csv(CSV_FILE, sep=',')
    train_df = df[df['Usage'] == 'Training']
    eval_df = df[df['Usage'] == 'PublicTest']
    return train_df, eval_df


def _preprocess_fer(df,
                    label_col='emotion',
                    feature_col='pixels'):
    labels, features = df.loc[:, label_col].values.astype(np.int32), [
        np.fromstring(image, np.float32, sep=' ')
        for image in df.loc[:, feature_col].values]

    labels = [keras.utils.to_categorical(l, num_classes=num_classes) for l in labels]

    features = np.stack((features,) * 3, axis=-1)
    features /= 255
    features = features.reshape(features.shape[0], img_width, img_height, 3)

    return features, labels

def build_model_original_layers(x_train, y_train, x_valid, y_valid):
    gen = ImageDataGenerator(
            rotation_range=40,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True,
            fill_mode='nearest')

    train_generator = gen.flow(x_train, y_train, batch_size=batch_size)
    predict_size_train = int(np.math.ceil(len(x_train) / batch_size))

    gen = ImageDataGenerator()
    valid_generator = gen.flow(x_valid, y_valid, batch_size=batch_size)
    predict_size_valid = int(np.math.ceil(len(x_valid) / batch_size))

    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same', input_shape=(img_width, img_height, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.3))

    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.3))

    model.add(Conv2D(128, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.3))

    model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
    model.add(Dense(256))
    model.add(Dropout(0.6))
    model.add(Dense(num_classes))
    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy', optimizer=keras.optimizers.Adam(lr=0.001), metrics=['accuracy'])
    model.summary()

    model.fit_generator(train_generator,
                        steps_per_epoch=predict_size_train * 3,
                        epochs=200,
                        validation_data=valid_generator,
                        validation_steps=predict_size_valid)

    score, acc = model.evaluate_generator(valid_generator, steps=predict_size_valid)

    return model, score, acc



def build_model_alternate_layers(x_train, y_train, x_valid, y_valid):
    gen = ImageDataGenerator(
            rotation_range=40,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True,
            fill_mode='nearest')

    train_generator = gen.flow(x_train, y_train, batch_size=batch_size)
    predict_size_train = int(np.math.ceil(len(x_train) / batch_size))

    gen = ImageDataGenerator()
    valid_generator = gen.flow(x_valid, y_valid, batch_size=batch_size)
    predict_size_valid = int(np.math.ceil(len(x_valid) / batch_size))

    model = Sequential()
    model.add(ZeroPadding2D((1, 1), input_shape=(img_height, img_width, 3)))
    model.add(Convolution2D(32, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(32, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(64, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(128, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(128, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(256, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(256, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(256, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    # model.add(Convolution2D(4096, (7, 7), activation='relu'))
    # model.add(Dropout(0.5))
    # model.add(Convolution2D(4096, (1, 1), activation='relu'))
    # model.add(Dropout(0.5))
    # model.add(Convolution2D(2622, (1, 1)))
    model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
    model.add(Dense(256))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes))
    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy', optimizer=keras.optimizers.Adam(lr=0.001), metrics=['accuracy'])
    model.summary()

    model.fit_generator(train_generator,
                        steps_per_epoch=predict_size_train * 3,
                        epochs=200,
                        validation_data=valid_generator,
                        validation_steps=predict_size_valid)

    score, acc = model.evaluate_generator(valid_generator, steps=predict_size_valid)

    return model, score, acc

def build_from_pretrained_model(x_train, y_train, x_valid, y_valid):
    gen = ImageDataGenerator(
            rotation_range=40,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True,
            fill_mode='nearest')

    train_generator = gen.flow(x_train, y_train, batch_size=batch_size)
    predict_size_train = int(np.math.ceil(len(x_train) / batch_size))

    gen = ImageDataGenerator()
    valid_generator = gen.flow(x_valid, y_valid, batch_size=batch_size)
    predict_size_valid = int(np.math.ceil(len(x_valid) / batch_size))

    input_tensor = Input(shape=(img_width, img_height, 3))
    vgg16 = VGG16(include_top=False, weights='imagenet', input_tensor=input_tensor)

    x = vgg16.output

    x = Flatten()(x)

    x = Dense(256)(x)
    x = Activation('relu')(x)
    x = Dropout(0.5)(x)

    x = Dense(num_classes)(x)
    x = Activation('softmax')(x)

    model = Model(inputs=[input_tensor], outputs=[x])
    model.summary()

    model.compile(loss='categorical_crossentropy',
                  optimizer=keras.optimizers.Adam(lr=0.0001),
                  metrics=['accuracy'])

    model.fit_generator(train_generator,
                        steps_per_epoch=predict_size_train * 3,
                        epochs=200,
                        validation_data=valid_generator,
                        validation_steps=predict_size_valid)



    score, acc = model.evaluate_generator(valid_generator, steps=predict_size_valid)

    return model, score, acc

# Ausführen des Trainingsschritts
def main(CSV_FILE, model_path):
    # Load fer data
    train_df, eval_df = _load_fer(CSV_FILE)

    # preprocess fer data
    x_train, y_train = _preprocess_fer(train_df)
    x_valid, y_valid = _preprocess_fer(eval_df)

    # print(x_train.shape[0], 'train samples')
    # print(x_valid.shape[0], 'valid samples')

    model, score1, acc1 = build_model_original_layers(x_train, y_train, x_valid, y_valid)
    model.save(model_path + 'original.h5')

    model, score2, acc2 = build_model_alternate_layers(x_train, y_train, x_valid, y_valid)
    model.save(model_path + 'alternate.h5')

    model, score3, acc3 = build_from_pretrained_model(x_train, y_train, x_valid, y_valid)
    model.save(model_path + 'pretrained.h5')


    print('Accuracy of model1:', acc1)
    print('Accuracy of model2:', acc2)
    print('Accuracy of model3:', acc3)

    print('done')


if __name__ == '__main__':
    CSV_FILE = "../../data/visuals/fer2013.csv"
    model_path = '../../data/models/visual_model_'
    main(CSV_FILE, model_path)
