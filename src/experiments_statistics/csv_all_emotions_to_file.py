from keras.preprocessing.image import img_to_array
from keras.models import load_model

import cv2
import numpy as np
import sys

import os
import collections
import pickle

# parameters for loading data and images

emotion_model_path = 'models/visual_model.h5'
#img_path = sys.argv[1]
samples = [10]#[1,2,3,4,5,6,7,
emotions = ['sad']#, 'angry', 'sad', 'neutral']

for sample in samples:
    for emotion in emotions:

        sampleDict = collections.defaultdict(list)
        dir_path = 'Emotions_Adina/'+emotion+'/'+str(sample)
        directory = os.fsencode(dir_path)

        for file in os.listdir(directory):

             filename = os.fsdecode(file)
             if filename.endswith(".png"):


                emotion_classifier = load_model(emotion_model_path, compile=False)
                EMOTIONS = ["angry","disgust","scared", "happy", "sad", "surprised","neutral"]


                #reading the frame
                orig_frame = cv2.imread(os.path.join(dir_path, filename))
                #frame = cv2.imread(img_path,0)

                roi = orig_frame.astype("float") / 255.0
                roi = img_to_array(roi)
                roi = np.expand_dims(roi, axis=0)
                preds = emotion_classifier.predict(roi)[0]

                i =0;
                while (i<EMOTIONS.__len__()):
                    emo = EMOTIONS[i]
                    pred = preds[i]
                    #print ('emo',EMOTIONS[i])
                    #print ('prob',preds[i])
                    sampleDict[emo].append(pred)
                    i+=1
        with open('A_statistics/allEmotions_' + str(sample) +'_'+ str(emotion) + '.txt', 'wb') as file:
            pickle.dump(sampleDict, file)

if (cv2.waitKey(2000) & 0xFF == ord('q')):
    sys.exit("Thanks")
    cv2.destroyAllWindows()





