import pickle
import json
import collections
from texttable import Texttable
from tabulate import tabulate
from collections import Counter


samples = [1,2,3,4,5,6,7,8,9,10]
emotions = ['happy','angry','sad', 'neutral']
method = 'max'
person = 'V'#'A','C','V'

thresha = 0.1#1
threshn = 0.8#0


#sample = 1
#emotion = 'happy'

t = Texttable()
t.add_row(['Sample', 'Emotion', 'Probabilities', 'Result'])


#thresholds = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
thresholds = [0,0.1,0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2,0.3]
thresholds = [1]

threshDict = {}

for thresh in thresholds:
    quality = 0
    qualityDict = {key: 0 for key in emotions}
    for sample in samples:
        for emotion in emotions:
            with open (person+'_statistics/allEmotions_'+str(sample)+'_'+emotion+'.txt', 'rb') as file:
                emoDict = pickle.loads(file.read())
            #resultDict1= collections.defaultdict(list)
            #resultDict2= collections.defaultdict(list)
            #resultDict3= collections.defaultdict(list)

            allEmoProbs = []

            if method == 'sum':
                for emo, probList in emoDict.items():
                        #method1 sumOverAllImages
                        emotion_sum = sum(emoDict[emo])
                        allEmoProbs.append((emo,emotion_sum))
                        allEmoProbs.sort(key=lambda x: x[1], reverse=True)
                        #resultDict1[sample].append([emotion, emotion_sum])

                if (emotion == allEmoProbs[0][0]):
                    quality +=1
                    qualityDict[emotion] += 0.1  # percentage -> 10 samples

                t.add_row([sample, emotion, allEmoProbs, max(allEmoProbs,key=lambda x:x[1])])

            elif method == 'max':

                zipped = list(zip(*list(emoDict.values())))
                maxList = [max(elem) for elem in zipped]
                keyList = list(emoDict.keys())

                maxIndex = [elem.index(max(elem)) for elem in zipped ]
                maxKey = [keyList[elem] for elem in maxIndex]

                result = list(zip(maxKey, maxList))

                countedEmotions = []
                allCount = len(result)

                for emot in list(emoDict.keys()):

                    counts = Counter(x[0] for x in result)
                    emoCount = (counts[emot])
                    emoPercentage = round(emoCount/allCount,2)
                    countedEmotions.append((emot,emoCount,emoPercentage))
                countedEmotions.sort(key=lambda x:x[1], reverse=True)

                if (countedEmotions[0][0]=="neutral" and countedEmotions[0][2] < threshn):
                    countedEmotions.insert(len(countedEmotions), countedEmotions.pop(0))

                for elem in countedEmotions:
                    if elem[0] == 'angry' and elem[2]> thresha:
                        countedEmotions.insert(0, countedEmotions.pop(countedEmotions.index(elem)))


                if (emotion == countedEmotions[0][0]):

                    quality +=1

                    qualityDict[emotion] += 0.1 #percentage -> 10 samples


                t.add_row([sample, emotion, result, countedEmotions])

    threshDict[thresh]=qualityDict
                #method3 higherThanCertainvValue

#with open('OverallEmotions.txt', 'w') as file:
#            json.dump(str(resultDict1),file)#, str(resultDict2)))


#print(t.draw())


qualityRate = quality/((len(samples)*len(emotions)))
print(qualityDict)
print(threshDict)
qualities = list(zip(qualityDict.keys(),qualityDict.values()))

with open(person+'_statistics/Table_'+method+'_'+str(qualityRate)+'_.txt', "w") as text_file:

    text_file.write(t.draw())

with open(person+'_statistics/Stats_'+method+'_'+'_.txt', "wb") as text_file:

    pickle.dump(qualities, text_file)

with open(person+'_statistics/threshDict_'+method+'_.txt', "wb") as text_file:

    pickle.dump(threshDict, text_file)



