import matplotlib.pyplot as plt
import numpy as np
import pickle
#import compute_overallEmotion

person = 'V'
method = 'max'
emotions = ['happy','angry','sad', 'neutral']

with open(person+'_statistics/threshDict_'+method+'_.txt', 'rb') as file:
    stats = pickle.loads(file.read())
thresholds = list(stats.keys())

ax = plt.subplot(111)

plt.title('Emotion Classification_'+person)
plt.xlabel('Threshold "Angry"')
plt.ylabel('Accuracy')

for emo in emotions:
    plt.plot(thresholds, [list(stats.values())[n][emo] for n in range(thresholds.__len__())], label=emo)

leg = plt.legend(loc='best', ncol=2, mode="expand", shadow=True, fancybox=True)
leg.get_frame().set_alpha(0.5)


plt.savefig(person+'_Statistics/Thresh_Plot_'+method+'_.png')
plt.show()

