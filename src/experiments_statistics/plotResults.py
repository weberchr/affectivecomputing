import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pickle

method = "max"
with open('A_statistics/Stats_' + method + '__.txt', 'rb') as file:
    adina = pickle.loads(file.read())

with open('V_statistics/Stats_' + method + '__.txt', 'rb') as file:
    vicky = pickle.loads(file.read())

#adina = [('angry',20), ('happy', 30), ('sad', 45), ('neutral', 79)]
#chris = [('angry',0.5), ('happy', 0.5), ('sad', 0.5), ('neutral', 0.5)]
#vicky = [('angry',21), ('happy', 50), ('sad', 41), ('neutral', 88)]
chris = []

#hmm = (1,2,3,4)

ind = np.arange(len(adina))  # the x locations for the groups
width = 0.2  # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(ind-width, np.array(list(elem[1] for elem in adina)), width, label='A')
rects2 = ax.bar(ind, np.array(list(elem[1] for elem in vicky)), width, label='V')
#rects3 = ax.bar(ind + width, np.array(list(elem[1] for elem in chris)), width, label='C')

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_ylabel('Scores')
ax.set_title('Emotion Detection Accuracy')
ax.set_xticks(ind)
ax.set_xticklabels(elem[0] for elem in adina)
ax.legend()


def autolabel(rects, xpos='center'):
    """
    Attach a text label above each bar in *rects*, displaying its height.

    *xpos* indicates which side to place the text w.r.t. the center of
    the bar. It can be one of the following {'center', 'right', 'left'}.
    """

    ha = {'center': 'center', 'right': 'left', 'left': 'right'}
    offset = {'center': 0, 'right': 1, 'left': -1}

    for rect in rects:
        height = rect.get_height()
        ax.annotate('{0:.2f}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(offset[xpos]*3, 3),  # use 3 points offset
                    textcoords="offset points",  # in both directions
                    ha=ha[xpos], va='bottom')


autolabel(rects1, "left")
autolabel(rects2, "right")

fig.tight_layout()
plt.savefig('Plot_'+method+'_.png')

plt.show()

