import os
import cv2
import csv
import numpy as np
import face_recognition #pip install cmake dlib face_recognition
import subprocess
import re
import shutil
from zipfile import ZipFile


outpath = '../../data/visuals/ravdess.csv'

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

def cmd_to_string(cmd):
    """remove unneded whitepsace from command strings"""
    return re.sub("""\\s+""", " ", cmd).strip()


def subprocess_cmd(command):
    print("running command: {}".format(cmd_to_string(command)))
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    out, _ = process.communicate(command.encode('utf-8'))
    print(out.decode('utf-8'))

def wgetGT(url, destination):
    print('updating zip file into current folder')
    wgetcmd = '''
    wget --content-disposition {link} -P {destination}
    '''.format(link=url, destination=destination)
    subprocess_cmd(wgetcmd)


def unpack(filedir, todir):
    with ZipFile(filedir, 'r') as myzip:
        print("unpacking {file} to {todir}"
              .format(file=filedir, todir=todir))
        myzip.extractall(todir)


def get_face(frame):
    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_frame = frame[:, :, ::-1]

    # Find all the faces in the current frame of video
    face_locations = face_recognition.face_locations(rgb_frame)

    # Display the results
    for top, right, bottom, left in face_locations:
        frame = frame[top:bottom, left:right]
    return frame


errorcount = 0
for i in range(1, 25):
    n = '{:02d}'.format(i)
    url = 'https://zenodo.org/record/1188976/files/Video_Speech_Actor_{}.zip?download=1'.format(n)

    downloadpath = 'download'
    zipdir = os.path.join(downloadpath, 'Video_Speech_Actor_{}.zip'.format(n))

    unpackpath = 'unpack'
    actorpath = os.path.join(unpackpath, 'Actor_{}'.format(n))

    wgetGT(url, downloadpath)
    unpack(zipdir, unpackpath)
    os.remove(zipdir)

    root, _, files = os.walk(actorpath).__next__()

    for fi, f in enumerate(files):
        prefix = 'Progress: ' + 'Actor_{}'.format(n)
        printProgressBar(fi + 1, len(files), prefix=prefix, suffix='Complete', length=50)

        file = os.path.join(root,f)

        f2, _ = os.path.splitext(f)
        identifiers = f2.split('-')
        emotion = '{:01d}'.format(int(identifiers[2]))
        intensity = '{:01d}'.format(int(identifiers[3]))
        actor = identifiers[6]
        repitition = identifiers[5]

        cap = cv2.VideoCapture(file)

        count = 0
        frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        while count < frame_count-1:
            count += 1
            c = '{:04d}'.format(count)
            ret, frame = cap.read()

            try:
                face = get_face(frame)

                width, height = 48, 48
                dim = (width, height)

                # resize image
                resized = cv2.resize(face, dim, interpolation=cv2.INTER_AREA)
                gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)

                data = np.array(gray.ravel()).tolist()
                data = ' '.join([str(i) for i in data])

                with open(outpath, mode='a', newline='') as csv_file:
                    fieldnames = ['emotion', 'intensity', 'pixels']
                    writer = csv.DictWriter(csv_file, fieldnames=fieldnames, delimiter=',')
                    writer.writerow({'emotion': emotion, 'intensity': intensity, 'pixels': data})
            except:
                errorcount += 1

        cap.release()

    shutil.rmtree(actorpath)

print('frames with errors:', errorcount)
