"""
Description: Train emotion classification model
"""

from keras.callbacks import CSVLogger, ModelCheckpoint, EarlyStopping
from keras.callbacks import ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
from keras.layers import Activation, Convolution2D, Dropout, Conv2D
from keras.layers import AveragePooling2D, BatchNormalization
from keras.layers import GlobalAveragePooling2D
from keras.models import Sequential
from keras.layers import Flatten
from keras.models import Model
from keras.layers import Input
from keras.layers import MaxPooling2D
from keras.layers import SeparableConv2D
from keras import layers
from keras.regularizers import l2
import pandas as pd
import cv2
import numpy as np

from keras.models import load_model
import os


dataset_path = '../../data/visuals/custom_images_training.csv'
#model_path = '../../data/models/fer2013Training/custom_img/_mini_XCEPTION.01-0.84.hdf5'

image_size=(48,48)
# parameters
batch_size = 32
num_epochs = 100
input_shape = (48, 48, 1)
validation_split = .2
verbose = 1
num_classes = 4
patience = 50
base_path = '../../data/models/fer2013Training/only_fer2013_mini_XCEPTION.53-0.76.hdf5'

l2_regularization=0.01


def load_fer2013():
    data = pd.read_csv(dataset_path)

    # 1 'Angry',  2 'Disgust',  3 'Fear',  4 'Happy',  5 'Sad',  6 'Surprise',  7 'Neutral'

    # data = data[data.emotion != 2] #drop 'Disgust'
    # data = data[data.emotion != 3] #drop 'Fear'
    # data = data[data.emotion != 6] #drop 'Surprise'
    #
    # #rename old labels to labels 1-4 - 1 Neutral, 2 Happy, 3 Sad, 4 Angry
    # data.loc[data['emotion'] == 4, 'emotion'] = 2
    # data.loc[data['emotion'] == 1, 'emotion'] = 4
    # data.loc[data['emotion'] == 7, 'emotion'] = 1
    # data.loc[data['emotion'] == 5, 'emotion'] = 3


    pixels = data['pixels'].tolist()
    width, height = 48, 48
    faces = []
    for pixel_sequence in pixels:
        face = [int(pixel) for pixel in pixel_sequence.split(' ')]
        face = np.asarray(face).reshape(width, height)
        face = cv2.resize(face.astype('uint8'),image_size)
        faces.append(face.astype('float32'))
    faces = np.asarray(faces)
    faces = np.expand_dims(faces, -1)
    emotions = pd.get_dummies(data['emotion']).as_matrix()
    return faces, emotions



def preprocess_input(x, v2=True):
    x = x.astype('float32')
    x = x / 255.0
    if v2:
        x = x - 0.5
        x = x * 2.0
    return x



# data generator
data_generator = ImageDataGenerator(
                        featurewise_center=False,
                        featurewise_std_normalization=False,
                        rotation_range=10,
                        width_shift_range=0.1,
                        height_shift_range=0.1,
                        zoom_range=.1,
                        horizontal_flip=True)
epoch = []
accuracy = []
accuracy_on_customset = []


# loading dataset
faces, emotions = load_fer2013()
faces = preprocess_input(faces)
num_samples, num_classes = emotions.shape
xtrain, xtest, ytrain, ytest = train_test_split(faces, emotions, test_size=0.2, shuffle=True)
valid_generator = data_generator.flow(xtest, ytest, batch_size=batch_size)


model = load_model(base_path, compile=True)

predict_size_valid = int(np.math.ceil(len(xtest) / batch_size))
score, acc = model.evaluate_generator(valid_generator, steps=predict_size_valid)

print(acc)