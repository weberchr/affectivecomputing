import numpy as np
import keras
from keras.models import load_model
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import Input
from keras.layers import Activation, Dropout, Flatten, Dense




emotion_model_path = '../../data/models/fer2013Training/only_fer2013_mini_XCEPTION.53-0.76.hdf5'
original_model = load_model(emotion_model_path)

batch_size = 32
img_width,img_height = 48, 48
num_classes = 4



def build_from_pretrained_model(x_train, y_train, x_valid, y_valid):
    gen = ImageDataGenerator(
            rotation_range=40,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True,
            fill_mode='nearest')

    train_generator = gen.flow(x_train, y_train, batch_size=batch_size)
    predict_size_train = int(np.math.ceil(len(x_train) / batch_size))

    gen = ImageDataGenerator()
    valid_generator = gen.flow(x_valid, y_valid, batch_size=batch_size)
    predict_size_valid = int(np.math.ceil(len(x_valid) / batch_size))

    input_tensor = Input(shape=(img_width, img_height, 3))
    # vgg16 = VGG16(include_top=False, weights='imagenet', input_tensor=input_tensor)

    vgg16 = original_model

    x = vgg16.output

    x = Flatten()(x)

    x = Dense(256)(x)
    x = Activation('relu')(x)
    x = Dropout(0.5)(x)

    x = Dense(num_classes)(x)
    x = Activation('softmax')(x)

    model = Model(inputs=[input_tensor], outputs=[x])
    model.summary()

    model.compile(loss='categorical_crossentropy',
                  optimizer=keras.optimizers.Adam(lr=0.0001),
                  metrics=['accuracy'])

    model.fit_generator(train_generator,
                        steps_per_epoch=predict_size_train * 3,
                        epochs=200,
                        validation_data=valid_generator,
                        validation_steps=predict_size_valid)



    score, acc = model.evaluate_generator(valid_generator, steps=predict_size_valid)

    return model, score, acc

